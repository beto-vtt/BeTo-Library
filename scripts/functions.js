'use strict';
(factory => {
    Hooks.once('init', () => {
        window.beto = factory();
    });
})(() => {
    const moduleResolvers = {};
    const modulePromises = {};
    const onCanvasReadyCallbacks = [];
    const canvasReadyPromise = new Promise(resolve => {
        Hooks.on('canvasReady', canvas => {
            resolve(canvas);
            onCanvasReadyCallbacks.forEach(callback => callback(canvas));
        });
    });

    const onCanvasReady = callback => {
        onCanvasReadyCallbacks.push(callback);
        if (canvas !== undefined) {
            setTimeout(() => {
                callback(canvas);
            });
        }
    };

    const calculateDistance = (tokenA, tokenB) => {
        return Math.min(...canvas.grid.measureDistances(
            [{ ray: new Ray(tokenA.center, tokenB.center) }, { ray: new Ray(tokenB.center, tokenA.center) }],
            { gridSpaces: true }
        ));
    };

    const getTokenForActor = actor => { // TODO @JB check what this does with multiple monsters of the same type
        return canvas.tokens.placeables.find(placeable => placeable.actor?.id === actor.id);
    };

    const getActivePlayerTokens = canvas => {
        if (canvas.tokens.controlled.length > 0) {
            return canvas.tokens.controlled;
        }
        return [canvas.tokens.placeables.find(placeable => placeable.actor?.id === game.user.character.id)];
    };

    const getOnlyInSet = set => {
        if (set.size !== 1) {
            throw new Error('The set does not have exactly 1 item');
        }
        let returnItem;
        set.forEach(item => returnItem = item);
        return returnItem;
    };

    const getModule = moduleName => {
        if (Object.hasOwnProperty.call(modulePromises, moduleName)) {
            return modulePromises[moduleName];
        } else {
            modulePromises[moduleName] = new Promise(resolve => {
                moduleResolvers[moduleName] = resolve;
            });
            return modulePromises[moduleName];
        }
    };

    const registerModule = (moduleName, module) => {
        if (Object.hasOwnProperty.call(modulePromises, moduleName) && moduleResolvers[moduleName] !== undefined) {
            moduleResolvers[moduleName](module);
            delete moduleResolvers[moduleName];
        } else {
            modulePromises[moduleName] = new Promise(resolve => {
                resolve(module);
            });
        }
        return module;
    };

    const asyncEvery = async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            const response = await callback(array[index], index, array);
            if (response === false) {
                return false;
            }
        }
        return true;
    };

    const asyncForEach = async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    };

    const beto = {
        getCanvas: () => canvasReadyPromise,
        onCanvasReady,
        calculateDistance,
        getOnlyInSet,
        getTokenForActor,
        getActivePlayerTokens,
        findTokenById: (canvas, id) => canvas.tokens.placeables.find(placeable => placeable.id === id),
        getReach: actor => actor.getFlag('beto-library', 'reach') ?? 5,
        canInteract: (actorToken, targetToken) => calculateDistance(actorToken, targetToken) <= beto.getReach(actorToken.actor),
        getModule,
        registerModule,
        asyncEvery,
        asyncForEach,
    };

    beto.registerModule('beto', beto);
    Hooks.callAll('beto.library.loaded', beto);

    return beto;
});
